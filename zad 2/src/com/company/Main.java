package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        // Initialize cards
        String[] deck = new String[52];

        String[] suits = {"Spades", "Clubs", "Hearts", "Diamonds"};
        String[] rank = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};

        // Randomly select a card
        String randomSuit = (suits[new Random().nextInt(suits.length)]);
        String randomRank = (rank[new Random().nextInt(rank.length)]);
        System.out.println(randomRank + " of " + randomSuit);

        // Display all the cards
        int d = 0;
        for (int i = 0; i < suits.length; i++){
            for (int j = 0; j < rank.length; j++){
                deck[d] = rank[j] + " of " + suits[i];
                System.out.println(deck[d]);
                d++;
            }
        }

        // Shuffle the cards
        for (int i = 0; i < deck.length; i++) {
            int index = (int)(Math.random() * deck.length);
            String temp = deck[i];
            deck[i] = deck[index];
            deck[index] = temp;
        }
        for (int i = 0; i < deck.length; i++){
            System.out.println(deck[i]);
        }
    }

}


