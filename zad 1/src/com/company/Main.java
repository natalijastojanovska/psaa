package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("String 1: ");
        String str1 = scan.nextLine();

        System.out.println("String 2: ");
        String str2 = scan.nextLine();

        if (str1.length() - str2.length() == 0) {
            char[] a1 = str1.toCharArray();
            char[] a2 = str2.toCharArray();
            Arrays.sort(a1);
            Arrays.sort(a2);

            if(Arrays.equals(a1, a2)) {
                System.out.println("The two strings are anagram of each other");
            }
            else {
                System.out.println("The two strings are not anagram of each other");
            }
        }
        else {
            System.out.println("The two strings are not anagram of each other");
        }



    }
}
